package ru.buzanov.tm.controller.face;

import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.dto.UserDTO;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.service.UserService;

import javax.faces.application.FacesMessage;

@Component("users")
@Scope("view")
public class Users {
    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    private UserDTO userDTO = new UserDTO();


    public String reg() {
        System.out.println("reg");
            final UserDTO user = new UserDTO();
            user.setPasswordHash(passwordEncoder.encode(userDTO.getPasswordHash()));
            user.setLogin(userDTO.getLogin());
            user.setName(userDTO.getName());
            user.getRoles().add(RoleType.USER);
        try {
            userService.load(user);
        } catch (Exception e) {
            PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage(e.getMessage()));
        } finally {
            userDTO = new UserDTO();
        }
        return "auth.xhtml?faces-redirect=true";
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}
