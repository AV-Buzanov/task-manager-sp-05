package ru.buzanov.tm.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.buzanov.tm.entity.Roles;
import ru.buzanov.tm.repository.RoleRepository;
import ru.buzanov.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service("userDetailService")
public class UserDetailServiceBean implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        final ru.buzanov.tm.entity.User user = userRepository.findByLogin(username);
        if (user == null)
            throw new UsernameNotFoundException("User not found");
        final List<String> roles = new ArrayList<>();
        for (Roles role : roleRepository.findAllByUserId(user.getId()))
            roles.add(role.getRoleType().name());
        final User.UserBuilder builder = User.withUsername(username)
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}));
        final CustomUser customUser = new CustomUser((User) builder.build());
        customUser.setUserId(user.getId());
        return customUser;
    }
}
