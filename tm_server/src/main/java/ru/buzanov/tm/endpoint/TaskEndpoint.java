package ru.buzanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.enumerated.Field;
import ru.buzanov.tm.security.CustomUser;
import ru.buzanov.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@Service
public class TaskEndpoint {
    @Autowired
    public TaskService taskService;

    @WebMethod
    @Nullable
    public Collection<TaskDTO> findAllT() throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        return taskService.findAll(user.getUserId());
    }

    @WebMethod
    @Nullable
    public TaskDTO findOneT(@Nullable String id) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        return taskService.findOne(user.getUserId(), id);
    }

    @WebMethod
    public boolean isNameExistT(@Nullable String name) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return false;
        return taskService.isNameExist(user.getUserId(), name);
    }

    @WebMethod
    @Nullable
    public String getListT() throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        return taskService.getList(user.getUserId());
    }

    @WebMethod
    @Nullable
    public String getIdByCountT(int count) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        return taskService.getIdByCount(user.getUserId(), count);
    }

    public void mergeT(@Nullable String id, @Nullable TaskDTO task) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return;
        taskService.merge(user.getUserId(), id, task);
    }

    @WebMethod
    @Nullable
    public TaskDTO removeT(@Nullable String id) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        taskService.remove(user.getUserId(), id);
        return null;
    }

    public void removeAllT() throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return;
        taskService.removeAll(user.getUserId());
    }

    @WebMethod
    @Nullable
    public Collection<TaskDTO> findByDescriptionT(@Nullable String desc) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        return taskService.findByDescription(user.getUserId(), desc);
    }

    @WebMethod
    @Nullable
    public Collection<TaskDTO> findByNameT(@Nullable String name) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        return taskService.findByName(user.getUserId(), name);
    }

    @WebMethod
    public @Nullable Collection<TaskDTO> findAllOrderedT(boolean dir, @NotNull Field field) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        return taskService.findAllOrdered(user.getUserId(), dir, field);
    }

    @WebMethod
    @Nullable
    public TaskDTO loadT(@Nullable TaskDTO entity) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return null;
        taskService.load(user.getUserId(), entity);
        return null;
    }

    @WebMethod
    public void loadListT(List<TaskDTO> list) throws Exception {
        CustomUser user;
        if ((user = getUser()) == null)
            return;
        taskService.load(user.getUserId(), list);
    }

    @Nullable
    private CustomUser getUser() {
        if (SecurityContextHolder.getContext().getAuthentication() == null)
            return null;
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (user instanceof CustomUser)
            return (CustomUser) user;
        return null;
    }
}
