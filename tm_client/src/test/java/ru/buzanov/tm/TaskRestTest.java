package ru.buzanov.tm;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.enumerated.Status;
import ru.buzanov.tm.rest.AuthResource;
import ru.buzanov.tm.rest.AuthResourceClient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Category(IntagratedTests.class)
public class TaskRestTest extends Assert {
    @Nullable
    private AuthResource authResource;

    @Before
    public void setUp() throws Exception {
        authResource = AuthResourceClient.getInstance("http://localhost:8080/rest");
        assertNotNull(authResource);
        authResource.register("testUser", "123456");
        authResource.auth("testUser", "123456");
    }

    @After
    public void tearDown() throws Exception {
        authResource.removeUser();
        authResource = null;
    }

    @Test
    public void testTaskCreate() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        authResource.mergeTask(task);
        @Nullable final TaskDTO testTask = authResource.getTask(task.getId());
        assertNotNull(testTask);
        assertEquals("not equals id", task.getId(), testTask.getId());
        assertEquals("not equals userId", authResource.user().getId(), testTask.getUserId());
        assertEquals("not equals Name", task.getName(), testTask.getName());
        assertEquals("not equals createDate", task.getCreateDate().toString(), testTask.getCreateDate().toString());
        assertEquals("not equals status", Status.PLANNED, testTask.getStatus());
    }

    @Test(expected = FeignException.class)
    public void testTaskCreateExistName() throws FeignException {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        authResource.mergeTask(task);
        @NotNull final TaskDTO testTask = new TaskDTO();
        testTask.setName("Name");
        authResource.mergeTask(testTask);
    }

    @Test
    public void testTaskMerge() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        task.setDescription("description");
        task.setStartDate(new Date(System.currentTimeMillis()));
        task.setFinishDate(new Date(System.currentTimeMillis()));
        authResource.mergeTask(task);
        task.setName("newName");
        task.setDescription("newDescription");
        task.setStartDate(new Date(System.currentTimeMillis()));
        task.setFinishDate(new Date(System.currentTimeMillis()));
        task.setStatus(Status.IN_PROGRESS);
        authResource.mergeTask(task);
        @Nullable final TaskDTO testTask = authResource.getTask(task.getId());
        checkEquals(testTask, task);
    }

    @Test
    public void testTaskFind() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        task.setDescription("description");
        task.setStartDate(new Date(System.currentTimeMillis()));
        task.setFinishDate(new Date(System.currentTimeMillis()));
        task.setStatus(Status.DONE);
        authResource.mergeTask(task);
        @Nullable final TaskDTO testTask = authResource.getTask(task.getId());
        checkEquals(testTask, task);
    }

    @Test
    public void testTaskFindAllSize() {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("Name");
        authResource.mergeTask(task);
        task = new TaskDTO();
        task.setName("Name2");
        authResource.mergeTask(task);
        task = new TaskDTO();
        task.setName("Name3");
        authResource.mergeTask(task);
        @Nullable final List<TaskDTO> list = new ArrayList<>(authResource.listTask());
        assertNotNull(list);
        assertEquals("size error", 3, list.size());
    }

    @Test
    public void testTaskRemove() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        authResource.mergeTask(task);
        authResource.removeTask(task.getId());
        @Nullable final TaskDTO testTask = authResource.getTask(task.getId());
        assertNull(testTask);
    }

    @Test(expected = FeignException.class)
    public void testTaskCreateUnauthorize() throws FeignException {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        AuthResource resource = AuthResourceClient.getInstance("http://localhost:8080/rest");
        resource.mergeTask(task);
    }

    @Test(expected = FeignException.class)
    public void testTaskFindUnauthorize() throws FeignException {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        authResource.mergeTask(task);
        AuthResource resource = AuthResourceClient.getInstance("http://localhost:8080/rest");
        @Nullable final TaskDTO testTask = resource.getTask(task.getId());
        assertNull(testTask);
    }

    @Test(expected = FeignException.class)
    public void testTaskRemoveUnauthorize() throws FeignException {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("Name");
        authResource.mergeTask(task);
        AuthResource resource = AuthResourceClient.getInstance("http://localhost:8080/rest");
        resource.removeTask(task.getId());
        @Nullable final TaskDTO testTask = authResource.getTask(task.getId());
        assertNotNull(testTask);
    }

    private void checkEquals(@Nullable final TaskDTO testTask, @Nullable final TaskDTO task) {
        assertNotNull(testTask);
        assertNotNull(task);
        assertEquals("not equals id", task.getId(), testTask.getId());
        assertEquals("not equals userId", authResource.user().getId(), testTask.getUserId());
        assertEquals("not equals Name", task.getName(), testTask.getName());
        assertEquals("not equals desc", task.getDescription(), testTask.getDescription());
        assertEquals("not equals createDate", task.getCreateDate().toString(), testTask.getCreateDate().toString());
        assertEquals("not equals startDate", task.getStartDate().toString(), testTask.getStartDate().toString());
        assertEquals("not equals finishDate", task.getFinishDate().toString(), testTask.getFinishDate().toString());
        assertEquals("not equals status", task.getStatus(), testTask.getStatus());
    }
}