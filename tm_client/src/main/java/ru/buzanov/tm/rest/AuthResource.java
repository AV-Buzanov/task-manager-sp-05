package ru.buzanov.tm.rest;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.buzanov.tm.dto.UserDTO;

@FeignClient("auth")
public interface AuthResource extends RestClient {
    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    ResponseEntity auth(@RequestHeader("login") String login,
                        @RequestHeader("password") String password);

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    UserDTO user();

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    ResponseEntity logout();

    @RequestMapping(value = "/profile/merge", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    UserDTO mergeProfile(@RequestBody final UserDTO user);

    @RequestMapping(value = "/profile/remove", method = RequestMethod.DELETE)
    ResponseEntity removeUser();

    @RequestMapping(value = "/register", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity register(@RequestHeader("login") String login,
                            @RequestHeader("password") String password);
}
