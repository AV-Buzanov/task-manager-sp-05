package ru.buzanov.tm.rest;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.buzanov.tm.dto.ProjectDTO;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.dto.UserDTO;

import java.util.Collection;

@FeignClient("rest")
public interface RestClient {
    @RequestMapping(value = "/project/findAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Collection<ProjectDTO> listProject();

    @RequestMapping(value = "/project/find/{id}", method = RequestMethod.GET)
    ProjectDTO getProject(@PathVariable("id") final String id);

    @RequestMapping(value = "/project/mergeAll", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    Collection<ProjectDTO> mergeAllProject(@RequestBody Collection<ProjectDTO> projects);

    @RequestMapping(value = "/project/merge", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ProjectDTO mergeProject(@RequestBody ProjectDTO project);

    @RequestMapping(value = "/project/remove/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity removeProject(@PathVariable("id") final String id);

    @RequestMapping(value = "/project/removeAll", method = RequestMethod.DELETE)
    ResponseEntity removeAllProject();

    @RequestMapping(value = "/task/findAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Collection<TaskDTO> listTask();

    @RequestMapping(value = "/task/find/{id}", method = RequestMethod.GET)
    TaskDTO getTask(@PathVariable("id") final String id);

    @RequestMapping(value = "/task/mergeAll", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    Collection<TaskDTO> mergeAllTask(@RequestBody Collection<TaskDTO> tasks);

    @RequestMapping(value = "/task/merge", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    TaskDTO mergeTask(@RequestBody TaskDTO task);

    @RequestMapping(value = "/task/remove/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity removeTask(@PathVariable("id") final String id);

    @RequestMapping(value = "/task/removeAll", method = RequestMethod.DELETE)
    ResponseEntity removeAllTask();

    @RequestMapping(value = "/user/findAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    Collection<UserDTO> listUser();

    @RequestMapping(value = "/user/find/{id}", method = RequestMethod.GET)
    UserDTO getUser(@PathVariable("id") final String id);

    @RequestMapping(value = "/user/mergeAll", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    Collection<UserDTO> mergeAllUser(@RequestBody Collection<UserDTO> users);

    @RequestMapping(value = "/user/merge", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    UserDTO mergeUser(@RequestBody UserDTO user);

    @RequestMapping(value = "/user/remove/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity removeUser(@PathVariable("id") final String id);

    @RequestMapping(value = "/user/removeAll", method = RequestMethod.DELETE)
    ResponseEntity removeAllUser();
}
