package ru.buzanov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ru.buzanov.tm.dto.ProjectDTO;
import ru.buzanov.tm.rest.*;
import ru.buzanov.tm.rest.template.AuthRestClient;
import ru.buzanov.tm.rest.template.ProjectRestClient;
import ru.buzanov.tm.rest.template.TaskRestClient;
import ru.buzanov.tm.rest.template.UserRestClient;

@Getter
@Setter
@Component
@NoArgsConstructor
public final class Bootstrap {
    @NotNull
    @Autowired
    private ProjectRestClient projectRestEndpoint;
    @Autowired
    private TaskRestClient taskRestEndpoint;
    @Autowired
    private UserRestClient userRestEndpoint;
    @Autowired
    private AuthRestClient authRestClient;

    public void start() {
    }
}