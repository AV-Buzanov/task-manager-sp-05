package ru.buzanov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlEnumValue;

public enum Status {
    @XmlEnumValue(value = "Запланированно")
    PLANNED("Запланированно"),
    @XmlEnumValue(value = "В процессе")
    IN_PROGRESS("В процессе"),
    @XmlEnumValue(value = "Завершено")
    DONE("Завершено");

    @NotNull private final String name;

    Status(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String displayName() {
        return name;
    }
}
